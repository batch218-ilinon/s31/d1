const http = require("http");

// Creates a variable "port" to store the port number
// 3000, 4000, 5000, 8000 - usually used for web development
const port = 3000;

const server = http.createServer(function(request, response){
	if(request.url == "/"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello World")
	}
	else if(request.url == "/greeting"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello Again")
	}
	else if(request.url == "/homepage"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("You are now in my homepage")
	}
	else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not available")
	}
});

server.listen(port);
console.log(`Server is now accessible at localhost:${port}`)